import 'dart:async';

import 'package:get/get.dart';
import 'package:ist_test_app/app/routes/app_pages.dart';

class SplashController extends GetxController {
  @override
  void onReady() {
    super.onReady();

    // add delay 2 seconds before moving to login 
    Timer(Duration(seconds: 2), () => Get.toNamed(Routes.LOGIN));
  }
}
