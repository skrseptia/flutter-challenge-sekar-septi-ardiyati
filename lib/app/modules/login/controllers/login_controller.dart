import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ist_test_app/app/modules/login/auth_model.dart';
import 'package:ist_test_app/app/modules/login/views/popup.dart';
import 'package:ist_test_app/app/routes/app_pages.dart';
import 'package:ist_test_app/app/services/api_client.dart';
import 'package:ist_test_app/app/services/api_constant.dart';
import 'package:ist_test_app/app/services/api_exception.dart';

class LoginController extends GetxController {
  var dio = Dio();
  var isLoading = true.obs;
  var isError = false.obs;

  final userIdController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  void onInit() {
    // Create default value for testing purpose
    userIdController.text = 'kminchelle';
    passwordController.text = '0lelplR';
    super.onInit();
  }

  @override
  void onClose() {
    userIdController.dispose();
    passwordController.dispose();
    super.onClose();
  }

  Future<void> login() async {
    var userId = userIdController.value.text;
    var password = passwordController.value.text;

    var payload = {
      "username": userId,
      "password": password,
    };

    try {
      final response =
          await ApiClient(dio).post(ApiConstant.login, data: payload);
      var login = Auth.fromJson(response.data);

      CustomDialog.loginSuccess(
          title: 'Success',
          message: 'Login Success',
          action: () {
            Get.toNamed(Routes.HOME);
          });
    } on DioError catch (e) {
      isLoading(false);
      isError(true);
      final errorMessage = ApiExceptions.fromDioError(e).toString();
      if (userId.isEmpty && password.isEmpty) {
        CustomDialog.loginFailed(
            title: 'Warning',
            message: 'User ID and Password cannot be empty',
            action: () {
              Get.back();
            });
        // Get.snackbar('Warning', 'Password cannot be empty');
      } else if (password.isEmpty) {
        CustomDialog.loginFailed(
            title: 'Warning',
            message: 'Password cannot be empty',
            action: () {
              Get.back();
            });
      } else if (userId.isEmpty) {
        CustomDialog.loginFailed(
            title: 'Warning',
            message: 'UserID cannot be empty',
            action: () {
              Get.back();
            });
      } else {
        CustomDialog.loginFailed(
            title: 'Warning',
            message: errorMessage,
            action: () {
              Get.back();
            });
      }
      debugPrint(errorMessage);
    }
  }
}
