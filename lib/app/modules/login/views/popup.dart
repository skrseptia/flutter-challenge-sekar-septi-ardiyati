import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CustomDialog {
  CustomDialog._();

  static Future<dynamic> loginSuccess({
    required String title,
    required String message,
    required action,
  }) {
    return showDialog(
      context: Get.context as BuildContext,
      barrierDismissible: true,
      builder: (context) => AlertDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30),
        ),
        title: Column(
          children: [
            Icon(
              Icons.check_circle_outline_sharp,
              size: 50,
            ),
            SizedBox(height: 10),
            Text(title),
          ],
        ),
        content: Text(
          message,
          textAlign: TextAlign.center,
        ),
        actions: <Widget>[
          Center(
            child: SizedBox(
              width: 120,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.purple),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                    ),
                  ),
                ),
                onPressed: action,
                child: const Text('Okay'),
              ),
            ),
          )
        ],
      ),
    );
  }

  static Future<dynamic> loginFailed({
    required String title,
    required String message,
    required action,
  }) {
    return showDialog(
      context: Get.context as BuildContext,
      barrierDismissible: true,
      builder: (context) => AlertDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30),
        ),
        title: Column(
          children: [
            Icon(
              Icons.highlight_remove,
              size: 50,
            ),
            SizedBox(height: 10),
            Text(title),
          ],
        ),
        content: Text(
          message,
          textAlign: TextAlign.center,
        ),
        actions: <Widget>[
          Center(
            child: SizedBox(
              width: 120,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.purple),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                    ),
                  ),
                ),
                onPressed: action,
                child: const Text('Okay'),
              ),
            ),
          )
        ],
      ),
    );
  }
}
