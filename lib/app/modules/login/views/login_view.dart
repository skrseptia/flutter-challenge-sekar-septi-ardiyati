import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/login_controller.dart';

class LoginView extends GetView<LoginController> {
  const LoginView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Image.asset('assets/images/header-login.png'),
                SizedBox(
                  width: 120,
                  height: 120,
                  child: Image.asset('assets/images/logo.png'),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(48.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Login',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                  ),
                  SizedBox(height: 16),
                  Text('Please sign in to continue.'),
                  SizedBox(height: 32),
                  Text("User ID"),
                  TextFormField(
                    controller: controller.userIdController,
                    decoration: const InputDecoration(
                        hintText: 'User ID',
                        border: UnderlineInputBorder(),
                        hintStyle: TextStyle(
                            fontStyle: FontStyle.italic, fontSize: 14)),
                  ),
                  SizedBox(height: 16),
                  Text("Password"),
                  TextFormField(
                    controller: controller.passwordController,
                    decoration: const InputDecoration(
                        hintText: 'Password',
                        border: UnderlineInputBorder(),
                        hintStyle: TextStyle(
                            fontStyle: FontStyle.italic, fontSize: 14)),
                  ),
                  SizedBox(height: 32),
                  Container(
                    alignment: Alignment.centerRight,
                    child: SizedBox(
                      width: 120,
                      child: ElevatedButton(
                        onPressed: controller.login,
                        style: ButtonStyle(
                           backgroundColor:  MaterialStateProperty.all(Colors.purple),
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(18.0),
                            ),
                            
                          ),
                        ),
                        child: const Text("LOGIN"),
                      ),
                    ),
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text('Dont have an account?'),
                  TextButton(
                      onPressed: () {},
                      style: TextButton.styleFrom(
                        foregroundColor: Colors.red,
                      ),
                      child: const Text('Sign Up'))
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
 
