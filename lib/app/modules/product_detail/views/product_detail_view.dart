import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/product_detail_controller.dart';

class ProductDetailView extends GetView<ProductDetailController> {
  const ProductDetailView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final c = Get.put(ProductDetailController());
    return Scaffold(
      appBar: AppBar(
        title: const Text('Product Detail'),
        backgroundColor: Colors.purple,
        centerTitle: true,
      ),
      body: Obx(
        () => c.isLoading.value
            ? Center(
                child: CircularProgressIndicator(),
              )
            : Center(
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                  child: Column(
                    children: [
                      Text(
                        c.product.value.title ?? '',
                        style: TextStyle(fontSize: 20),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        " ${c.product.value.brand ?? ''}",
                        style: TextStyle(fontSize: 20),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      SizedBox(
                        width: 250,
                        child:
                            Image.network(c.product.value.thumbnail.toString()),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        '\$ ${c.product.value.price.toString()}',
                        style: TextStyle(fontSize: 20),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        c.product.value.description ?? '',
                        style: TextStyle(fontSize: 20),
                      ),
                    ],
                  ),
                ),
              ),
      ),
    );
  }
}
