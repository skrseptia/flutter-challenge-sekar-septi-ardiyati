import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:ist_test_app/app/modules/home/products_model.dart';
import 'package:ist_test_app/app/services/api_client.dart';
import 'package:ist_test_app/app/services/api_constant.dart';
import 'package:ist_test_app/app/services/api_exception.dart';

class ProductDetailController extends GetxController {
  var dio = Dio();
  var isLoading = true.obs;
  var isError = false.obs;

  var product = Product().obs;

  @override
  void onInit() {
    var id = Get.arguments;
    getProduct(id);
    super.onInit();
  }

  Future<void> getProduct(int id) async {
    try {
      isLoading(true);
      final response = await ApiClient(dio).get(
        '${ApiConstant.product}/$id',
      );

      var data = Product.fromJson(response.data);
      product.value = data;
      isLoading(false);
    } on DioError catch (e) {
      isLoading(false);
      isError(true);
      final errorMessage = ApiExceptions.fromDioError(e).toString();
      Get.snackbar('Error', errorMessage);
    }
  }
}
