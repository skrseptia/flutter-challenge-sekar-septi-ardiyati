import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:ist_test_app/app/modules/home/products_model.dart';
import 'package:ist_test_app/app/routes/app_pages.dart';
import 'package:ist_test_app/app/services/api_client.dart';
import 'package:ist_test_app/app/services/api_constant.dart';

class HomeController extends GetxController {
  var dio = Dio();
  var isLoading = true.obs;
  var isError = false.obs;

  RxList<Product> productSearch = List<Product>.empty().obs;
  RxList<Product> product = RxList<Product>.empty();
  TextEditingController searchProductKeyword = TextEditingController();

  final pageSize = 10;
  final PagingController<int, Product> pagingController =
      PagingController(firstPageKey: 0);

  @override
  void onInit() {
    pagingController.addPageRequestListener((pageKey) {
      getProducts(pageKey);
    });
    super.onInit();
  }

  @override
  void onClose() {
    pagingController.dispose();
    super.onClose();
  }

  Future<void> getProducts(int pageKey) async {
    var params = {
      "limit": pageSize,
      "skip": pageKey,
    };

    try {
      isLoading(true);
      final response = await ApiClient(dio)
          .get(ApiConstant.product, queryParameters: params);
      var products = Products.fromJson(response.data);

      var newItems = products.products;

      final isLastPage = newItems!.length < pageSize;
      if (isLastPage) {
        pagingController.appendLastPage(newItems);
      } else {
        final nextPageKey = pageKey + newItems.length;
        pagingController.appendPage(newItems, nextPageKey);
      }

      isLoading(false);
    } on DioError catch (e) {
      isLoading(false);
      isError(true);
      pagingController.error = e;
    }
  }

  void goToDetail(int id) {
    Get.toNamed(Routes.PRODUCT_DETAIL, arguments: id);
  }

  void searchProduct() {
    productSearch(product
        .where((item) => item.title!
            .toLowerCase()
            .contains(searchProductKeyword.text.toLowerCase()))
        .toList());

    update();
  }
}
