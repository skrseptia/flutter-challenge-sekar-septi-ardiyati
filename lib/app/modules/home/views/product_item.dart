import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ist_test_app/app/modules/home/controllers/home_controller.dart';

class ProductItem extends StatelessWidget {
  final int id;
  final String title;
  final String description;
  final String imageURL;
  final int price;
  final int stock;

  const ProductItem(
      {super.key,
      required this.id,
      required this.title,
      required this.description,
      required this.imageURL,
      required this.price,
      required this.stock});

  @override
  Widget build(BuildContext context) {
    final c = Get.put(HomeController());
    return ListTile(
      leading:  SizedBox(width: 80, child: Image.network(imageURL)),
      title: Text(title),
      subtitle: Text(description),
      trailing: Text('\$ $price'),
      onTap: () => c.goToDetail(id),
    );
  }
}
