import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:ist_test_app/app/modules/home/controllers/home_controller.dart';
import 'package:ist_test_app/app/modules/home/products_model.dart';
import 'package:ist_test_app/app/modules/home/views/product_item.dart';

class HomeView extends GetView<HomeController> {
  const HomeView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    var c = Get.put(HomeController());
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Colors.purple,
        fontFamily: 'Georgia',
      ),
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: const Text('HomeView'),
          centerTitle: true,
          backgroundColor: Colors.purple,
        ),
        body: PagedListView<int, Product>.separated(
          pagingController: c.pagingController,
          builderDelegate: PagedChildBuilderDelegate<Product>(
            animateTransitions: true,
            itemBuilder: (context, item, index) => ProductItem(
              id: item.id ?? 0,
              title: item.title ?? '',
              description: item.description ?? '',
              imageURL: item.thumbnail ?? '',
              price: item.price ?? 0,
              stock: item.stock ?? 0,
            ),
          ),
          separatorBuilder: (context, index) => const Divider(),
        ),
      ),
    );
  }
}
