class ApiConstant {
  ApiConstant._();
  static const baseURL = "https://dummyjson.com/";
  static const int connectionTimeout = 15000;
  static const int receiveTimeout = 15000;

  static const login = "auth/login";
  static const product = "products";
}
