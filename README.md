# Frontend Flutter Challenge

Sekar Septi Ardiyati (skrseptia@gmail.com)

## How To Run This App
Install dependency:
```
flutter pub get
```
Run Application:
```
flutter run
```
